<?php
use App\Shop\Data\Producers\ProducerContainer;
use App\Shop\Data\Producers\ProducerItem;
use App\Shop\ShopApi;
use System\Rest\Authorization\BasicAuthorization;
use System\Rest\Exceptions\ApiException;

/**
 * Paths define
 */
define('BASE_DIR', dirname(__DIR__));

/**
 * Boolean debug status
 */
define('DEBUG', true);

/**
 * Auto loader PSR-4
 */
require_once('../system/Autoloader.php');

/**
 * Create new autoload class instance
 */
$autoloader = new Autoloader();

/**
 * Add namespace and it's mapped include directories
 * Example: $autoloader->addNamespace('Lib\\', 'system/');
 */
$autoloader->addNamespace('App\\', 'app/');
$autoloader->addNamespace('System\\', 'system/');

/**
 * Registering auto loader
 */
$autoloader->register();

try
{
    /**
     * ShopApi for example usage of API Classes
     */
    $shopApi = new ShopApi(
        new BasicAuthorization("rest", "vKTUeyrt"),             // Example Authorization Module <- We can add more in the future in easy way :-)
        "http://grzegorz.demos.i-sklep.pl/rest_api/shop_api/v1" // Base URL of the API
    );                                                          // (There is restriction to only one url per Api Object because I don't really know if we need more of it in the same API Object?)
                                                                // Usually we need only one URL Path at the same time

    echo "<pre>";
    echo "All items: " . $shopApi->GetAll(new ProducerContainer())->count() . "\n\n";

    $producer = new ProducerItem();
    $producer->setName("I-Systems");
    $producer->setSiteUrl("http://i-systems.pl");
    $producer->setLogoFilename("i-systems.png");
    $producer->setOrdering(1);
    $producer->setSourceId("");

    $result = $shopApi->CreateOne($producer, true);

    echo "Inserted item result: \n\n";
    var_dump(json_encode($result));
    echo "\n";

    echo "All items after creating new one: " . $shopApi->GetAll(new ProducerContainer())->count();
    echo "</pre>";
} catch (ApiException $apiException) {
    if(DEBUG) {
        echo "<h2>Api Exception occurred</h2>";
        echo "Exception Message: \"" . $apiException->getMessage() . "\".";
        echo "<br><hr>";
        echo "Api Information:";
        echo "<pre>";
        echo "HTTP Code: " . $apiException->getHttpCode() . "\n";
        echo "Request ID: " . $apiException->getRequestId() . "\n";
        echo "Reason Code: " . $apiException->getReasonCode() . "\n";
        echo "Messages:\n";

        foreach ($apiException->getErrorMessagesBag() as $errorMessage) {
            echo "\t--> " . $errorMessage . "\n";
        }

        echo "</pre>";
    }
    else {
        echo "<h2>Api Exception occurred</h2><hr />";
        echo "<pre>";
        echo "HTTP Code: " . $apiException->getHttpCode();
        echo "Request ID: " . $apiException->getRequestId();
        echo "</pre>";
    }
} catch (Throwable $throwable) {
    if(DEBUG) {
        echo "<h2>Throwable</h2><hr />";
        echo "<pre>";
        echo "File: " . $throwable->getFile() . "\n";
        echo "Message: " . $throwable->getMessage() . "\n";
        echo "Stack Trace: \n" . $throwable->getTraceAsString() . "\n";
        echo "</pre>";
    }
    else {
        echo "<h2>Exception occurred</h2>";
    }
}
