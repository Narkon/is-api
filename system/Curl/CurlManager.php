<?php
namespace System\Curl;

/**
 * Class CurlManager
 * @package System\Curl
 */
class CurlManager
{
    /**
     * @var resource
     */
    private $curl;

    /**
     * @var array
     */
    private $headers;

    /**
     * @var array
     */
    private $responseInfo;

    /**
     * @var array
     */
    private $responseHeaders;

    /**
     * @var bool
     */
    private $executed;

    /**
     * CurlManager constructor.
     */
    public function __construct()
    {
        $this->init();
        $this->headers = [];
        $this->responseInfo = [];
        $this->responseHeaders = [];
        $this->executed = false;
    }

    /**
     * Initialize the CURL
     * @param bool $forceRecreate
     */
    public function init(bool $forceRecreate = false) : void
    {
        if($forceRecreate || !is_resource($this->curl)) {
            $this->curl = curl_init();
            $this->executed = false;
        }
    }

    /**
     * Returns the actual Curl resource
     * @return resource of type curl
     */
    public function getCurl()
    {
        return $this->curl;
    }

    /**
     * @param string $url
     */
    public function setUrl(string $url) : void
    {
        curl_setopt($this->curl, CURLOPT_URL, $url);
    }

    /**
     * @param bool $enable
     */
    public function setSslVerifyPeer(bool $enable) : void
    {
        curl_setopt($this->curl, CURLOPT_SSL_VERIFYPEER, $enable);
    }

    /**
     * @param int $timeout
     */
    public function setTimeout(int $timeout = 30) : void
    {
        curl_setopt($this->curl, CURLOPT_TIMEOUT, $timeout);
    }

    /**
     * @param bool $enable
     */
    public function setReturnHeaders(bool $enable) : void
    {
        curl_setopt($this->curl, CURLOPT_HEADER, $enable);
    }

    /**
     * @param bool $enable
     */
    public function setReturnTransfer(bool $enable) : void
    {
        curl_setopt($this->curl, CURLOPT_RETURNTRANSFER, $enable);
    }

    /**
     * Sets the redirection status to enabled or disabled and sets its max count
     * @param bool $enable
     * @param int $maxRedirects
     */
    public function setRedirection(bool $enable, int $maxRedirects) : void
    {
        curl_setopt($this->curl, CURLOPT_FOLLOWLOCATION, $enable);
        curl_setopt($this->curl, CURLOPT_MAXREDIRS, $maxRedirects);
    }

    /**
     * @param array $headers
     */
    public function setRawHeaders(array $headers) : void
    {
        curl_setopt($this->curl, CURLOPT_HTTPHEADER, $headers);
    }

    /**
     * Sets CURL headers
     */
    public function setHeaders() : void
    {
        $this->setRawHeaders($this->headers);
    }

    /**
     * Adds new header to headers array. We should use this function if we don't want to override default headers
     * @param $key
     * @param $value
     */
    public function addHeader($key, $value) : void
    {
        $this->headers[strtolower($key)] = $value;
    }

    /**
     * Flushes headers array
     */
    public function flushHeaders() : void
    {
        $this->headers = [];
    }

    /**
     * Gets the appropriate info from response info array
     * @param $key
     * @return null|string
     */
    public function getResponseInfo($key) : ?string
    {
        if(isset($this->responseInfo[$key])) {
            return $this->responseInfo[$key];
        }

        return null;
    }

    /**
     * Gets the appropriate response header
     * @param $key
     * @return null|string
     * @throws \Exception
     */
    public function getResponseHeader($key) : ?string
    {
        if(!$this->executed) {
            throw new \Exception("Cannot get response header before request execution");
        }

        if(isset($this->responseHeaders[$key])) {
            return $this->responseHeaders[$key];
        }

        return null;
    }

    /**
     * Prepares POST Request
     * @param array $data
     */
    public function preparePostRequest(array $data) : void
    {
        if(empty($data)) {
            throw new \InvalidArgumentException("Provided data is empty");
        }

        $data = json_encode($data);

        $this->addHeader("Content-Length", strlen($data));

        curl_setopt($this->curl, CURLOPT_POST, true);
        curl_setopt($this->curl, CURLOPT_POSTFIELDS, $data);
    }

    /**
     * Executing the CURL request and returns response as bool or response string
     * @return bool|string
     */
    public function exec()
    {
        $response = curl_exec($this->curl);

        // Get CURL Transfer Information
        $this->prepareResponseInfo();
        $this->parseResponseHeaders($response);

        // Get only body from response string and cut off the header
        $body = substr($response, $this->responseInfo['header_size']);

        // Set execution status
        $this->executed = true;

        // Close CURL Handle
        $this->close();

        return $body;
    }

    /**
     * Closes the CURL Handle
     */
    public function close() : void
    {
        curl_close($this->curl);
    }

    /**
     * Prepares array of information got from CURL Transfer
     */
    private function prepareResponseInfo() : void
    {
        $responseInfo = curl_getinfo($this->curl);

        if($responseInfo != false) {
            $this->responseInfo = $responseInfo;
        }
    }

    /**
     * Parses CURL response headers to array
     * @param string $response
     */
    private function parseResponseHeaders(string $response) : void
    {
        $httpHeaders = substr($response, 0, strpos($response, "\r\n\r\n"));
        $headersChunks = explode("\r\n", $httpHeaders);

        foreach ($headersChunks as $header => $value) {
            if ($header === 0) {
                $value = explode(" ", $value);
                $this->responseHeaders['http_code'] = (int)$value[1];
                $this->responseHeaders['http_version'] = $value[0];
            }
            else {
                list($key, $value) = explode(': ', $value);

                $this->responseHeaders[$key] = $value;
            }
        }
    }
}