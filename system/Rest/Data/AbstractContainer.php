<?php
namespace System\Rest\Data;

/**
 * Class ContainerAbstract
 * @package System\Rest\Data
 */
abstract class AbstractContainer implements \JsonSerializable
{
    /**
     * @var string
     */
    private $apiResource;

    /**
     * @var \stdClass|array|null
     */
    private $data;

    /**
     * ContainerAbstract constructor.
     * @param string $resource
     * @param \stdClass|null $data
     */
    public function __construct(string $resource, ?\stdClass $data = null)
    {
        $this->setData($data);
        $this->setApiResource($resource);
    }

    /**
     * @return string
     */
    public function getApiResource(): string
    {
        return $this->apiResource;
    }

    /**
     * @param string $apiResource
     */
    public function setApiResource(string $apiResource) : void
    {
        if(substr($apiResource, 0, 1) !== "/") {
            $apiResource = "/" . $apiResource;
        }

        $this->apiResource = $apiResource;
    }

    /**
     * @return array|null
     */
    public function getData(): ?array
    {
        return $this->data;
    }

    /**
     * @param \stdClass $data
     */
    public function setData(?\stdClass $data) : void
    {
        if($data !== null) {
            $this->data = array_values(get_object_vars($data))[0];

            if(is_array($this->data)) {
                for ($i = 0; $i < count($this->data); $i++) {
                    $this->data[$i] = get_object_vars($this->data[$i]);
                }
            }
            else {
                $this->data = [get_object_vars($this->data)];
            }
        }
        else {
            $this->data = $data;
        }
    }

    /**
     * @param $key
     * @param $value
     * @throws \Exception
     */
    public function setItem($key, $value) : void
    {
        if(!isset($this->data[$key])) {
            $this->data[$key] = $value;
        }
        else {
            throw new \Exception("Cannot set item on key " . $key . " because there is already item assigned");
        }
    }

    /**
     * @param string $key
     * @param string $value
     * @throws \Exception
     */
    public function overrideItem(string $key, string $value) : void
    {
        $this->data[$key] = $value;
    }

    /**
     * @param string $key
     * @return array|null
     */
    public function getItem(string $key) : ?array
    {
        if(isset($this->data[$key])) {
            return $this->data[$key];
        }

        return null;
    }

    /**
     * Searching the Container data for given key and value
     * @param string $key
     * @param string $value
     * @return array|null Null if noting was found
     */
    public function findItemByKey(string $key = 'id', string $value) : ?array
    {
        if($this->getData() !== null && count($this->getData()) > 1) {
            $items = $this->getData();
            foreach ($items as $itemKey => $itemValue) {
                if($itemValue[$key] == $value) {
                    return $items[$itemKey];
                }
            }
        }

        return null;
    }

    /**
     * @return int
     */
    public function count() : int
    {
        return count($this->getData());
    }

    /**
     * Serializing
     */
    public function jsonSerialize()
    {
        return $this->getData();
    }
}