<?php
namespace System\Rest\Data;

/**
 * Class AbstractContainerItem
 * @package System\Rest\Data
 */
abstract class AbstractContainerItem implements \JsonSerializable
{
    /**
     * @var array|\stdClass|null
     */
    private $data;

    /**
     * @var string
     */
    private $apiResource;

    /**
     * @var string
     */
    private $dataPrefix;

    /**
     * AbstractContainerItem constructor.
     * @param string $resource
     * @param string $dataPrefix
     */
    public function __construct(string $resource, string $dataPrefix)
    {
        $this->setApiResource($resource);
        $this->setDataPrefix($dataPrefix);
    }

    /**
     * @return string
     */
    public function getApiResource(): string
    {
        return $this->apiResource;
    }

    /**
     * @param string $apiResource
     */
    protected function setApiResource(string $apiResource)
    {
        if(substr($apiResource, 0, 1) !== "/") {
            $apiResource = "/" . $apiResource;
        }

        $this->apiResource = $apiResource;
    }

    /**
     * @return string
     */
    protected function getDataPrefix(): string
    {
        return $this->dataPrefix;
    }

    /**
     * @param string $dataPrefix
     */
    protected function setDataPrefix(string $dataPrefix)
    {
        $this->dataPrefix = $dataPrefix;
    }

    /**
     * @return array
     */
    public function getData() : array
    {
        return [$this->getDataPrefix() => $this->data];
    }

    /**
     * @param mixed $data
     */
    public function setData(?\stdClass $data) : void
    {
        if($data !== null) {
            $this->data = array_values(get_object_vars($data))[0];

            if(is_array($this->data)) {
                for ($i = 0; $i < count($this->data); $i++) {
                    $this->data[$i] = get_object_vars($this->data[$i]);
                }
            }
            else {
                $this->data = [get_object_vars($this->data)];
            }
        }
        else {
            $this->data = $data;
        }
    }

    /**
     * @param $key
     * @param $value
     */
    protected function setItem($key, $value) : void
    {
        $this->data[$key] = $value;
    }

    /**
     * @param $key
     * @return mixed
     */
    protected function getItem($key) : mixed
    {
        return $this->data[$key];
    }
}