<?php
namespace System\Rest\Api;

use System\Curl\CurlManager;
use System\Rest\Authorization\AbstractAuthorization;
use System\Rest\Exceptions\ApiException;

/**
 * Class AbstractApi
 *
 * @package System\Rest\Api
 */
abstract class AbstractApi implements ApiInterface
{
    /**
     * @var bool
     */
    protected $followRedirects = true;

    /**
     * @var int
     */
    protected $redirectsMaxCount = 3;

    /**
     * @var string
     */
    protected $baseUrl = "";

    /**
     * @var AbstractAuthorization
     */
    protected $authorization;

    /**
     * @var CurlManager
     */
    protected $curlManager;

    /**
     * Const request types
     */
    protected const
        GET = 0,
        POST = 1,
        DELETE = 2,
        PUT = 3,
        PATCH = 4;

    /**
     * AbstractApi constructor.
     *
     * @param AbstractAuthorization $authorization
     * @param string $baseUrl
     */
    public function __construct(AbstractAuthorization $authorization, string $baseUrl)
    {
        $this->curlManager = new CurlManager();
        $this->authorization = $authorization;

        // Sets the CURL Manager for the Authorization Module
        $this->authorization->setCurlManager($this->curlManager);

        // Using function because of checks inside of it
        $this->setBaseUrl($baseUrl);
    }

    /**
     * Global method used for calling request
     * @param int|string $method
     * @param string $path
     * @param array $data
     * @return ApiResponse
     * @throws \Exception
     */
    protected function request(int $method, string $path, array $data) : ApiResponse
    {
        // Initialize Curl Handle
        $this->curlManager->init();

        // Set ssl verify peer status
        $this->curlManager->setSslVerifyPeer(false);

        // Add the default content-type
        $this->curlManager->addHeader("Content-Type", "application/json");

        // Curl transfer options
        $this->curlManager->setReturnHeaders(true);
        $this->curlManager->setRedirection($this->followRedirects, $this->redirectsMaxCount);

        // Avoid displaying raw curl response
        $this->curlManager->setReturnTransfer(true);

        // Authorize API Client
        $this->authorization->authorize();

        // Map the request method and do
        $this->mapMethod($method, $path, $data);

        // Set all headers
        $this->curlManager->setHeaders();

        // Execute the curl
        $response = $this->curlManager->exec();

        // If request failed
        if($response === false || $this->curlManager->getResponseInfo('http_code') >= 400) {
            throw new \Exception("CURL request failed with HTTP code: #" . $this->curlManager->getResponseInfo("http_code"));
        }

        // Prepare Api Response
        $apiResponse = new ApiResponse(
            $this->curlManager->getResponseHeader("http_code"),
            $response,
            $this->curlManager->getResponseHeader("X-API-RequestIdentifier")
        );

        if(!$apiResponse->isSuccess()) {
            throw new ApiException($apiResponse, "API Request has failed");
        }

        return $apiResponse;
    }

    /**
     * Map method to miscellaneous function
     *
     * @param int $method
     * @param string $path
     * @param array $data
     * @throws \Exception
     */
    private function mapMethod(int $method, string $path, array $data) : void
    {
        switch ($method)
        {
            case self::GET:
                if(empty($data)) {
                    $this->curlManager->setUrl($this->baseUrl . $path);
                }
                else {
                    $query = http_build_query($data);
                    $this->curlManager->setUrl($this->baseUrl . $path . $query);
                }
                break;
            case self::POST:
                $this->curlManager->setUrl($this->baseUrl . $path);
                $this->curlManager->preparePostRequest($data);
                break;
            case self::DELETE:
                // Not implemented
                break;
            case self::PUT:
                // Not implemented
                break;
            case self::PATCH:
                // Not implemented
                break;
            default:
                throw new \Exception("Unexpected request method exceeded");
        }
    }

    /**
     * @param AbstractAuthorization $authorization
     */
    public function setAuthorization(AbstractAuthorization $authorization) : void
    {
        $this->authorization = $authorization;
    }

    /**
     * Returns if follow redirects is enabled or not
     *
     * @return bool
     */
    protected function isFollowRedirects(): bool
    {
        return $this->followRedirects;
    }

    /**
     * @param bool $followRedirects
     */
    protected function setFollowRedirects(bool $followRedirects) : void
    {
        $this->followRedirects = $followRedirects;
    }

    /**
     * @return int
     */
    protected function getRedirectsMaxCount(): int
    {
        return $this->redirectsMaxCount;
    }

    /**
     * @param int $redirectsMaxCount
     */
    protected function setRedirectsMaxCount(int $redirectsMaxCount) : void
    {
        $this->redirectsMaxCount = $redirectsMaxCount;
    }

    /**
     * @return string
     */
    protected function getBaseUrl(): string
    {
        return $this->baseUrl;
    }

    /**
     * @param string $baseUrl
     */
    public function setBaseUrl(string $baseUrl) : void
    {
        if(!filter_var($baseUrl, FILTER_VALIDATE_URL)) {
            throw new \InvalidArgumentException("Base Url is not correct url address");
        }

        if(substr($baseUrl, strlen($baseUrl) - 1, 1) === "/") {
            $baseUrl = substr($baseUrl, 0, strlen($baseUrl) - 1);
        }

        $this->baseUrl = $baseUrl;
    }

    /**
     * Specified GET type for requesting resource
     *
     * @param string $path
     * @param array $data
     * @return ApiResponse
     */
    public function get(string $path, array $data = []) : ApiResponse
    {
        return $this->request(self::GET, $path, $data);
    }

    /**
     * Specified POST type for requesting resource
     *
     * @param string $path
     * @param array $data
     * @return ApiResponse
     */
    public function post(string $path, array $data) : ApiResponse
    {
        return $this->request(self::POST, $path, $data);
    }

    /**
     * Specified DELETE type for requesting resource
     *
     * @param string $path
     * @param array $data
     * @return ApiResponse
     * @throws \Exception
     */
    public function delete(string $path, array $data) : ApiResponse
    {
        throw new \Exception("Not implemented");
    }

    /**
     * Specified PUT type for requesting resource
     *
     * @param string $path
     * @param array $data
     * @return ApiResponse
     * @throws \Exception
     */
    public function put(string $path, array $data) : ApiResponse
    {
        throw new \Exception("Not implemented");
    }

    /**
     * Specified PATCH type for requesting resource
     *
     * @param string $path
     * @param array $data
     * @return ApiResponse
     * @throws \Exception
     */
    public function patch(string $path, array $data) : ApiResponse
    {
        throw new \Exception("Not implemented");
    }
}