<?php
namespace System\Rest\Api;

/**
 * Interface APIInterface
 * @package System\Rest\Api
 */
interface ApiInterface
{
    /**
     * Specified GET type for requesting resource
     *
     * @param string $path
     * @param array $data
     * @return ApiResponse
     */
    public function get(string $path, array $data = []) : ApiResponse;

    /**
     * Specified POST type for requesting resource
     *
     * @param string $path
     * @param array $data
     * @return ApiResponse
     */
    public function post(string $path, array $data) : ApiResponse;

    /**
     * Specified DELETE type for requesting resource
     *
     * @param string $path
     * @param array $data
     * @return ApiResponse
     */
    public function delete(string $path, array $data) : ApiResponse;

    /**
     * Specified PUT type for requesting resource
     *
     * @param string $path
     * @param array $data
     * @return ApiResponse
     */
    public function put(string $path, array $data) : ApiResponse;

    /**
     * Specified PATCH type for requesting resource
     *
     * @param string $path
     * @param array $data
     * @return ApiResponse
     */
    public function patch(string $path, array $data) : ApiResponse;
}