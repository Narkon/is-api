<?php

namespace System\Rest\Api;
use stdClass;

/**
 * Class ApiResponse
 * @package System\Rest\Api
 */
class ApiResponse
{
    /**
     * @var int
     */
    private $code;

    /**
     * @var stdClass
     */
    private $data;

    /**
     * @var stdClass
     */
    private $error;

    /**
     * @var string
     */
    private $id;

    /**
     * @var string
     */
    private $version;

    /**
     * @var bool
     */
    private $success;

    /**
     * ApiResponse constructor.
     * @param int $code
     * @param string $json
     * @param string $id
     */
    public function __construct(int $code, string $json, string $id)
    {
        $this->code = $code;
        $this->id = $id;

        $this->parseResponse($json);
    }

    /**
     * Parses response json and populates the ApiResponse
     * @param $json
     */
    private function parseResponse($json) : void
    {
        if(empty($json)) {
            return;
        }

        $decodedJson = json_decode($json);

        // Populate ApiResponse Items
        foreach ($decodedJson as $key => $value) {
            $this->{$key} = $value;
        }
    }

    /**
     * @return int
     */
    public function getCode(): int
    {
        return $this->code;
    }

    /**
     * @return stdClass
     */
    public function getData(): stdClass
    {
        return $this->data;
    }

    /**
     * @return stdClass
     */
    public function getError(): stdClass
    {
        return $this->error;
    }

    /**
     * @return string
     */
    public function getId(): string
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getVersion(): string
    {
        return $this->version;
    }

    /**
     * @return bool
     */
    public function isSuccess(): bool
    {
        return $this->success;
    }
}