<?php
namespace System\Rest\Exceptions;

use System\Rest\Api\ApiResponse;
use stdClass;
use Throwable;

/**
 * Class ApiException
 * @package System\Rest\Exceptions
 */
class ApiException extends \Exception
{
    /**
     * @var stdClass
     */
    private $apiResponse;

    /**
     * ApiException constructor.
     * @param ApiResponse $apiResponse
     * @param string $message
     * @param Throwable|null $previous
     */
    public function __construct(ApiResponse $apiResponse, $message = "", Throwable $previous = null)
    {
        $this->apiResponse = $apiResponse;

        parent::__construct($message, $apiResponse->getCode(), $previous);
    }

    /**
     * @return string
     */
    public function getRequestId() : string
    {
        return $this->apiResponse->getId();
    }

    /**
     * @return int
     */
    public function getHttpCode() : int
    {
        return $this->apiResponse->getCode();
    }

    /**
     * @return string
     */
    public function getReasonCode() : string
    {
        return $this->apiResponse->getError()->reason_code;
    }

    /**
     * @return array
     */
    public function getErrorMessagesBag() : array
    {
        return $this->apiResponse->getError()->messages;
    }
}