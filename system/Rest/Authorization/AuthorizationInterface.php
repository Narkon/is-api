<?php
namespace System\Rest\Authorization;

/**
 * Class AbstractAuthorization
 * @package System\Rest\Authorization
 */
interface AuthorizationInterface
{
    /**
     * Authorization method used to authorize the curl request
     *
     * @return mixed
     */
    public function authorize() : void;
}