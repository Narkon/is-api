<?php
namespace System\Rest\Authorization;

/**
 * Class BasicAuthorization
 * @package System\Rest\Authorization
 */
class BasicAuthorization extends AbstractAuthorization
{
    /**
     * @var string
     */
    private $username;

    /**
     * @var string
     */
    private $password;

    /**
     * BasicAuthorization constructor.
     * @param string $username
     * @param string $password
     */
    public function __construct(string $username, string $password)
    {
        $this->username = $username;
        $this->password = $password;
    }

    /**
     * Authorizing using the basic authorization
     */
    final public function authorize() : void
    {
        if(!isset($this->curlManager)) {
            throw new \Exception("CurlManager is not set properly!");
        }

        curl_setopt($this->curlManager->getCurl(), CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
        curl_setopt($this->curlManager->getCurl(), CURLOPT_USERPWD, "$this->username:$this->password");
    }
}