<?php
namespace System\Rest\Authorization;

use System\Curl\CurlManager;

/**
 * Class AbstractAuthorization
 * @package System\Rest\Authorization
 */
abstract class AbstractAuthorization implements AuthorizationInterface
{
    /**
     * @var CurlManager
     */
    protected $curlManager;

    /**
     * Sets the CurlManager
     *
     * @param CurlManager $curlManager
     */
    public function setCurlManager(CurlManager $curlManager) : void
    {
        $this->curlManager = $curlManager;
    }
}