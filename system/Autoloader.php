<?php

/**
 * Class Autoloader provides functionality to load classes with PSR-4 standard.
 */
class Autoloader
{
    /**
     * @var array
     */
    private $container;

    /**
     * @var string
     */
    private $fileExtension = ".php";

    /**
     * Autoloader constructor.
     */
    public function __construct()
    {
        $this->container = [];
    }

    /**
     * Adding specified namespace and directory
     * @param $namespace
     * @param $includeDirectory
     */
    public function addNamespace(string $namespace, string $includeDirectory) : void
    {
        if(!isset($this->container[$namespace])) {
            $this->container[$namespace] = $includeDirectory;
        }
    }

    /**
     * Removing specified namespace with it's directory
     * @param $namespace
     */
    public function removeNamespace(string $namespace) : void
    {
        if(isset($this->container[$namespace])) {
            unset($this->container[$namespace]);
        }
    }

    /**
     * Registering the autoloader
     */
    public function register() : void
    {
        spl_autoload_register([$this, 'loader']);
    }

    /**
     * Unregistering autoloader
     */
    public function unregister() : void
    {
        spl_autoload_unregister([$this, 'loader']);
    }

    /**
     * Autoloading function provides functionality to load classes with PSR-4 standard
     * @param $class
     */
    private function loader(string $class) : void
    {
        foreach ($this->container as $item => $value) {
            if($namespaceCheck = substr($class, 0, strlen($item)) !== $item) {
                continue;
            }

            $path = BASE_DIR . DIRECTORY_SEPARATOR . $value .
                str_replace('\\', DIRECTORY_SEPARATOR, str_replace($item, '', $class)) .
                $this->fileExtension;

            if(file_exists($path)) {
                require_once $path;
            }
        }
    }
}