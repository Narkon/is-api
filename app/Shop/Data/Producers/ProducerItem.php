<?php
namespace App\Shop\Data\Producers;

use System\Rest\Data\AbstractContainerItem;

/**
 * Class ProducerItem
 * @package App\Shop\Data\Producers
 */
class ProducerItem extends AbstractContainerItem
{
    /**
     * ProducerItem constructor.
     */
    public function __construct()
    {
        $resource = "producers";
        $prefixData = "producer";

        parent::__construct($resource, $prefixData);
    }

    /**
     * @param string $name
     * @throws \Exception
     */
    public function setName(string $name) : void
    {
        if(strlen($name) == 0) {
            throw new \Exception("Parameter Name cannot be empty");
        }

        $this->setItem('name', $name);
    }

    /**
     * @return string
     */
    public function getName() : string
    {
        return $this->getItem('name');
    }

    /**
     * @param string $siteUrl
     * @throws \Exception
     */
    public function setSiteUrl(string $siteUrl)
    {
        if(strlen($siteUrl) == 0) {
            throw new \Exception("Parameter Site Url cannot be empty");
        }

        $this->setItem('site_url', $siteUrl);
    }

    /**
     * @return string
     */
    public function getSiteUrl() : string
    {
        return $this->getItem('site_url');
    }

    /**
     * @param string $logoFilename
     * @throws \Exception
     */
    public function setLogoFilename(string $logoFilename)
    {
        if(strlen($logoFilename) == 0) {
            throw new \Exception("Parameter Logo Filename cannot be empty");
        }

        $this->setItem('logo_filename', $logoFilename);
    }

    /**
     * @return string
     */
    public function getLogoFilename() : string
    {
        return $this->getItem('logo_filename');
    }

    /**
     * @param int $id
     * @throws \Exception
     */
    public function setId(?int $id)
    {
        if($id < 0) {
            throw new \Exception("Parameter Id cannot be lower than 0");
        }

        $this->setItem('id', $id);
    }

    /**
     * @return int
     */
    public function getId() : ?int
    {
        return (int)$this->getItem('id');
    }

    /**
     * @param int $ordering
     * @throws \Exception
     */
    public function setOrdering(int $ordering)
    {
        if($ordering < 0) {
            throw new \Exception("Parameter Ordering cannot be lower than 0");
        }

        $this->setItem('ordering', $ordering);
    }

    /**
     * @return int
     */
    public function getOrdering() : int
    {
        return (int)$this->getItem('ordering');
    }

    /**
     * @param string $sourceId
     * @throws \Exception
     */
    public function setSourceId(string $sourceId)
    {
        $this->setItem('source_id', $sourceId);
    }

    /**
     * @return string
     */
    public function getSourceId() : string
    {
        return $this->getItem('source_id');
    }

    /**
     * Specify data which should be serialized to JSON
     * @link http://php.net/manual/en/jsonserializable.jsonserialize.php
     * @return mixed data which can be serialized by <b>json_encode</b>,
     * which is a value of any type other than a resource.
     * @since 5.4.0
     */
    public function jsonSerialize()
    {
        return $this->getData();
    }
}