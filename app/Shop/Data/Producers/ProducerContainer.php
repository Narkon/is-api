<?php
namespace App\Shop\Data\Producers;

use System\Rest\Data\AbstractContainer;

/**
 * Class ProducerContainer
 * @package App\Shop\Data
 */
class ProducerContainer extends AbstractContainer
{
    /**
     * ProducerContainer constructor.
     * @param null|\stdClass $data
     */
    public function __construct(?\stdClass $data = null)
    {
        $resource = "producers";

        parent::__construct($resource, $data);
    }
}