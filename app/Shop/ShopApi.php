<?php
namespace App\Shop;

use App\Shop\Data\Producers\ProducerContainer;
use System\Rest\Api\AbstractApi;
use System\Rest\Authorization\AbstractAuthorization;
use System\Rest\Data\AbstractContainer;
use System\Rest\Data\AbstractContainerItem;

/**
 * Class ShopApi
 * @package App
 */
class ShopApi extends AbstractApi
{
    /**
     * ShopApi constructor.
     *
     * @param AbstractAuthorization $authorization
     * @param string $baseUrl
     */
    public function __construct(AbstractAuthorization $authorization, string $baseUrl)
    {
        parent::__construct($authorization, $baseUrl);
    }

    /**
     * Returns populated collection with all data from API Resource
     * @param AbstractContainer $container
     * @return AbstractContainer
     */
    public function GetAll(AbstractContainer $container)
    {
        $response = $this->get($container->getApiResource());
        $container->setData($response->getData());

        return $container;
    }

    /**
     * Creates one item and returns it
     * @param AbstractContainerItem $item
     * @param bool $returnItem
     * @return AbstractContainerItem|null
     */
    public function CreateOne(AbstractContainerItem $item, bool $returnItem = false) : ?AbstractContainerItem
    {
        $response = $this->post($item->getApiResource(), $item->getData());

        if($returnItem) {
            $item->setData($response->getData());
            return $item;
        }

        return null;
    }
}